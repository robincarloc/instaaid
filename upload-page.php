<?php

  if(isset($_POST['filesToUpload'])) {
    foreach($_POST['filesToUpload'] as $file) {
      $type   =   $file['type'];
      $size   =   $file['size'];
      $name   =   $file['name'];
      $error  =   $file['error'];
      $temp   =   $file['tmp_name'];
      /* if this image was not removed from the list of selected files */

      if($type == 'image/jpeg' || $type == 'image/jpg' || $type == 'image/gif' || $type == 'image/png' && $error == 0) {
        if($size < 1048576) {
          $filename = $this->StringHandler->createFileName($name);
          $assetsPath = 'photos' . DS . $url . DS . 'images' . DS . 'assets';
          $filePath = $assetsPath . DS . $filename;
          
          /* initialize folder, to create folders that don't exist */
          $this->DirectoryHandler->init_folder($assetsPath, WWW);
          $request = move_uploaded_file($temp, WWW . DS . $filePath);
          
          if($request)
            $this->Session->setFlash('Image successfully uploaded.', 'default', array(), 'success');
          else
            $this->Session->setFlash('A problem occured while uploading the image.', 'default', array(), 'warning');
          
         // $this->clean_redirect( 'websites', 'upload_image', true, $url );
        }
      }

    }
  }

?>